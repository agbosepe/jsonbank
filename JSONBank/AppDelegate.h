//
//  AppDelegate.h
//  BitboticBank
//
//  Created by Daniel on 20/03/15.
//  Copyright (c) 2015 Dolmake. All rights reserved.
//

@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

