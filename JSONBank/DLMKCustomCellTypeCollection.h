//
//  DLMKtransactionTableViewCell.h
//  BitboticBank
//
//  Created by Daniel on 20/03/15.
//  Copyright (c) 2015 Dolmake. All rights reserved.
//

@import Foundation;
@import UIKit;

@interface DLMKCustomCellTypeCollection : NSObject

@property (strong, nonatomic, readonly) NSArray* arrayOfClasses;
+(instancetype) customCellTypeCollectionWithArray:(NSArray*) arrayOfClasses;

-(CGFloat) height;
-(NSString*) cellSectionTitle;
-(NSString*) cellId;


-(NSString*) cellSectionTitleForIndex: (NSUInteger)index;
-(NSString*) cellIdForIndex: (NSUInteger) index;

//TableView Cells
-(CGFloat) heightForIndex:(NSUInteger) index;
-(void) registerNibsForTableView:(UITableView*) tableView;
-(UITableViewCell*) cellForTableView:(UITableView*) tableView atIndex:(NSUInteger) index;

@end
