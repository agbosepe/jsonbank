//
//  DLMKtransactionTableViewCell.h
//  BitboticBank
//
//  Created by Daniel on 20/03/15.
//  Copyright (c) 2015 Dolmake. All rights reserved.
//

@import Foundation;
@class DLMKtransaction;


@interface DLMKModelServer : NSObject

+(instancetype) SINGLETON;

@property (nonatomic) NSUInteger transactionCount;
@property (nonatomic, strong) NSDictionary* transactionsModel;




-(void)loadTransactions:(void (^)(void))callback;
-(DLMKtransaction*) transactionAtIndex:(NSUInteger)index;
-(DLMKtransaction*) transactionBySKU:(NSString*)sku;


@end
