//
//  DLMKtransactionTableViewCell.h
//  BitboticBank
//
//  Created by Daniel on 20/03/15.
//  Copyright (c) 2015 Dolmake. All rights reserved.
//

#import "DLMKModelServer.h"
#import "DLMKtransaction.h"
#define DLMK_TRANSACTIONS_URL @"http://polar-ridge-7192.herokuapp.com/transactions.json"

@interface DLMKModelServer ()

@property (nonatomic, strong) NSArray* SKUList;

@end

@implementation DLMKModelServer

dispatch_queue_t queue;

static DLMKModelServer* s_instance;

#pragma mark - Singleton Methods

+ (id)SINGLETON {
    static DLMKModelServer *sharedModelServer = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedModelServer = [[self alloc] init];
    });
    return sharedModelServer;
}

#pragma mark - Properties
-(NSUInteger) transactionCount{
    return self.transactionsModel != nil ? self.transactionsModel.count : 0;
}

#pragma mark - Init

- (id)init {
    if (self = [super init]) {
        queue = dispatch_queue_create("com.dolmake.queue", nil);        
    }
    return self;
}


#pragma mark - Instance Methods

-(DLMKtransaction*) transactionAtIndex:(NSUInteger)index{
    NSString* sku = self.SKUList[index];
    return [self transactionBySKU:sku];
}
-(DLMKtransaction*) transactionBySKU:(NSString*)sku{
    return[self.transactionsModel valueForKey:sku ];
}

-(void)loadTransactions:(void (^)(void))callback{
    
    //Using GCD
    dispatch_async(queue, ^{
        
        NSError* error = nil;
        NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:DLMK_TRANSACTIONS_URL]];
        NSDictionary* json = nil;
        if (data != nil){
           json  = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if (json != nil){
                
                
                NSMutableDictionary* transactions = [NSMutableDictionary dictionaryWithCapacity:32];
                
                //Parse json
                for (NSDictionary* key in json) {
                    NSString* sku = [key valueForKey:@"sku"];
                    NSString* currency = [key valueForKey:@"currency"];
                    NSNumber* amount = [key valueForKey:@"amount"];
                    
                    DLMKtransaction* transaction = [transactions valueForKey:sku];
                    if (transaction == nil){
                        transaction = [DLMKtransaction transactionWithSKU:sku];
                        [transactions  setObject:transaction forKey:sku ];
                    }
                    
                    [transaction addCurrency:currency amount:amount];
                    
                }
                
                self.transactionsModel = transactions;
                
                NSMutableArray* skuList = [NSMutableArray arrayWithCapacity:32];
                for(NSString* key in self.transactionsModel){
                    [skuList addObject:key ];
                }
                
                //sort SKUList
                self.SKUList = [skuList sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];                
                
            }
        }
        
        //return to main queue
        dispatch_async(dispatch_get_main_queue(), ^{
            callback();
        });
        
    });

}


@end













