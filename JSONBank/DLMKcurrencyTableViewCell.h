//
//  DLMKcurrencyTableViewCell.h
//  BitboticBank
//
//  Created by Daniel on 20/03/15.
//  Copyright (c) 2015 Dolmake. All rights reserved.
//

@import UIKit;

@interface DLMKcurrencyTableViewCell : UITableViewCell

+(CGFloat)height;
+(NSString*)cellId;

@property (weak, nonatomic) IBOutlet UILabel *lbCurrency;
@property (weak, nonatomic) IBOutlet UILabel *lbAmount;

@end
