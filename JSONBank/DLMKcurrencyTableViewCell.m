//
//  DLMKcurrencyTableViewCell.m
//  BitboticBank
//
//  Created by Daniel on 20/03/15.
//  Copyright (c) 2015 Dolmake. All rights reserved.
//

#import "DLMKcurrencyTableViewCell.h"

@implementation DLMKcurrencyTableViewCell

#pragma mark - Class Methods
+(CGFloat)height{
    return 44.0f;
}
+(NSString*)cellId{
    return [self description];
}

#pragma mark - Instance Methods
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
