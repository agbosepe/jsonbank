//
//  DLMKtransaction.h
//  BitboticBank
//
//  Created by Daniel on 20/03/15.
//  Copyright (c) 2015 Dolmake. All rights reserved.
//

@import Foundation;

@interface DLMKtransaction : NSObject

+(instancetype) transactionWithSKU:(NSString*)sku;

-(id) initWithSKU:(NSString*)sku;

@property (nonatomic, copy) NSString* SKU;
@property (nonatomic, strong) NSMutableDictionary* currenciesIndexed;

-(void) addCurrency:(NSString*) currency amount:(NSNumber*)amount;

@end
