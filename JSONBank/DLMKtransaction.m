//
//  DLMKtransaction.m
//  BitboticBank
//
//  Created by Daniel on 20/03/15.
//  Copyright (c) 2015 Dolmake. All rights reserved.
//

#import "DLMKtransaction.h"

@implementation DLMKtransaction

#pragma mark - Class methods
+(instancetype) transactionWithSKU:(NSString*)sku{
    return [[DLMKtransaction alloc] initWithSKU:sku ];
    
}

#pragma mark - Init
-(id) initWithSKU:(NSString*)sku{
    if (self = [super init]){
        _SKU = sku;
        _currenciesIndexed = [NSMutableDictionary dictionaryWithCapacity:0];
    }
    return self;
}

#pragma mark - Instance methods
-(void) addCurrency:(NSString*) currency amount:(NSNumber*)amount{    
   
    NSNumber* accumAmount = [self.currenciesIndexed valueForKey:currency];
    if (accumAmount == nil)
        accumAmount = @(0);
    accumAmount=  @(accumAmount.floatValue + amount.floatValue);
    [self.currenciesIndexed setObject:accumAmount forKey:currency ];
    
}

@end





