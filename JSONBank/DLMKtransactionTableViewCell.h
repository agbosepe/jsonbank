//
//  DLMKtransactionTableViewCell.h
//  BitboticBank
//
//  Created by Daniel on 20/03/15.
//  Copyright (c) 2015 Dolmake. All rights reserved.
//

@import UIKit;
@class DLMKtransaction;

@interface DLMKtransactionTableViewCell : UITableViewCell

+(CGFloat)height;
+(NSString*)cellId;

@property (nonatomic, strong) DLMKtransaction* transactionModel;
@property (weak, nonatomic) IBOutlet UILabel *lbSKU;

@end
