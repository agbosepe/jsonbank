//
//  DLMKtransactionTableViewCell.m
//  BitboticBank
//
//  Created by Daniel on 20/03/15.
//  Copyright (c) 2015 Dolmake. All rights reserved.
//

#import "DLMKtransactionTableViewCell.h"
#import "DLMKtransaction.h"

@implementation DLMKtransactionTableViewCell


#pragma mark - Class Methods
+(CGFloat)height{
    return 74.0f;
}
+(NSString*)cellId{
    return [self description];
}

#pragma mark - Properties
-(void) setTransactionModel:(DLMKtransaction *)transactionModel{
    _transactionModel = transactionModel;
    self.lbSKU.text = _transactionModel.SKU;
}

#pragma mark - Instance Methods
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
