//
//  DLMKtransactionTableViewController.h
//  BitboticBank
//
//  Created by Daniel on 20/03/15.
//  Copyright (c) 2015 Dolmake. All rights reserved.
//
@import UIKit;
@class DLMKtransaction;

@interface DLMKtransactionTableViewController : UITableViewController

@property (strong, nonatomic) DLMKtransaction* detailItem;

@end
