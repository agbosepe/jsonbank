//
//  DLMKtransactionTableViewController.m
//  BitboticBank
//
//  Created by Daniel on 20/03/15.
//  Copyright (c) 2015 Dolmake. All rights reserved.
//

#import "DLMKtransactionTableViewController.h"
#import "DLMKtransaction.h"
#import "DLMKCustomCellTypeCollection.h"
#import "DLMKcurrencyTableViewCell.h"

@interface DLMKtransactionTableViewController ()

@property (nonatomic, strong) DLMKCustomCellTypeCollection* cells;

@end

@implementation DLMKtransactionTableViewController

#pragma mark - Properties

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        self.navigationItem.title = self.detailItem.SKU;
        //self.detailDescriptionLabel.text = [self.detailItem description];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    //Load & register Cells
    NSArray* cellTypes = @[[DLMKcurrencyTableViewCell class]];
    self.cells = [DLMKCustomCellTypeCollection customCellTypeCollectionWithArray:cellTypes];
    [self.cells registerNibsForTableView:self.tableView ];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    //return self.detailItem != nil ?  [self.detailItem.currencies count ] : 0;
    return self.detailItem != nil ? [[self.detailItem.currenciesIndexed allKeys ] count] :0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DLMKcurrencyTableViewCell *cell = (DLMKcurrencyTableViewCell *)[self.cells cellForTableView:tableView atIndex:0];
    
    //Non grouped currencies
    /*
    NSArray* currency = [self.detailItem.currencies objectAtIndex:indexPath.row ];
    
        cell.lbCurrency.text =currency[0];
    cell.lbAmount.text =[NSString stringWithFormat:@"%@", currency[1] ];
     */
    
    NSArray* currencies = [self.detailItem.currenciesIndexed allKeys ];
    NSString* key = currencies[indexPath.row];
    cell.lbCurrency.text = key;
    cell.lbAmount.text =[NSString stringWithFormat:@"%@", [self.detailItem.currenciesIndexed valueForKey:key ] ];
    
    
    return cell;
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self.cells heightForIndex:0 ];
}



@end
