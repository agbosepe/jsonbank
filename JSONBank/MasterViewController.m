//
//  MasterViewController.m
//  BitboticBank
//
//  Created by Daniel on 20/03/15.
//  Copyright (c) 2015 Dolmake. All rights reserved.
//

#import "MasterViewController.h"
#import "DLMKModelServer.h"
#import "DLMKCustomCellTypeCollection.h"
#import "DLMKtransactionTableViewCell.h"
#import "DLMKtransactionTableViewController.h"

@interface MasterViewController ()

@property (nonatomic, strong) DLMKCustomCellTypeCollection* cells;

@end

@implementation MasterViewController

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    */
    
    self.navigationItem.title = @"Transactions";

    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(loadTransactions:)];
    self.navigationItem.rightBarButtonItem = refreshButton;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    //Load & register Cells
    NSArray* cellTypes = @[[DLMKtransactionTableViewCell class]];
    self.cells = [DLMKCustomCellTypeCollection customCellTypeCollectionWithArray:cellTypes];
    [self.cells registerNibsForTableView:self.tableView ];
    
    //Load Transactions
    [self loadTransactions:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadTransactions:(id)sender {
    [[DLMKModelServer SINGLETON] loadTransactions:^{
        [self.tableView reloadData];
    } ];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        DLMKtransaction *transaction = [[DLMKModelServer SINGLETON] transactionAtIndex:indexPath.row ];
        DLMKtransactionTableViewController *controller = (DLMKtransactionTableViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:transaction];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.cells.arrayOfClasses count ];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[DLMKModelServer SINGLETON] transactionCount ];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.cells cellForTableView:tableView atIndex:0];
    DLMKtransaction* transaction = [[DLMKModelServer SINGLETON] transactionAtIndex:indexPath.row ];
    [cell setValue:transaction forKey:@"transactionModel"];

    
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self.cells heightForIndex:0];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"showDetail" sender:self];
}


@end
