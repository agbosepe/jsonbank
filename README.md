# README #

Applicación de ejemplo para una prueba de nivel para acceso a oferta laboral.

Tiempo estimado: 3:15

### Tareas ###

*  1.- Acceder a un webservice de forma asíncrona sin bloquear la aplicación
*  2.- Recibir y pasear el JSON recibido
*  3.- Agruparlo por Transacciones ( el id de cada transacción es el SKU)
*  4.- Al clickar en cada transacción se accede a los movimientos en moneda (Currency)
*  5.- Agrupar los movimientos de cada transacción por moneda y sumar el total del movimiento en la transacción por moneda.

Extra points:
*  6.- Botón en la barra de navegación que al pulsar se llame de nuevo al webservice y cargue un nuevo grupo de transacciones.
*  7.- Ordenar los resultados.